# Preliminary: Forking

First, ensure that you fork this project. When working with this project, try to ensure that any issues
or merge requests that you submit are *to your fork*.

Forking means making a whole new repository, as a copy of the original. It's what you might do if you 
wanted to create a whole new project, taking the code in a different direction than the original. It 
also gives you full "owner" access to the new copy, giving you the power to do things that you might 
not have been able to do to the original project.

In this case, you need to fork so that you can add code to the new repository and practice submitting 
issues and merging branches, while still leaving this original repository untouched for the next user.

You can fork a project on GitLab by going to the project home page (click "Project" at the top of the 
navigation bar on the left), and then finding the 'fork' link at the top right. It's right next to 'clone'. 
(See the [GitLab help page](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork).)


# Scrabble Score

The software requirement in this project is to write a function that takes a word as input, and computes 
the scrabble score for that word. The file [scrabble_score.py](scrabble_score.py) contains the function, called 'score'. It's 
currently empty; the definition needs to be filled in. And [scrable_score_test.py](scrable_score_test.py) contains tests you can 
use to see if you've written the definition correctly.

This project will introduce a workflow for logging the issues and best practices
for getting potential fixes merged into the project.

You'll need to: 
1. Fork the project, as above.
1. Clone the repository to your PC (See [Project 1](https://gitlab.com/ct-starter-guide/project1) for instructions, and make sure you're cloning from *your new fork*)
2. Run the tests, to discover that the function doesn't work. (Not surprising, since it's empty).
3. Raise an issue on the repository page on GitLab (explaining that the function doesn't currently work)
4. Create a new branch, so that you can modify the code without affecting the master branch.
5. Write the actual code! 
6. Submit a merge request (assuming that your code now passes the tests).

On a real project, this would then be followed by code review, where someone else checks your work, then 
finally by merging your changes back into the master branch if they passed the review.

## Running the tests
In order to test our [scrabble scorer](scrabble_score.py) works as expected, tests have been
written that use the [pytest framework](https://docs.pytest.org/en/latest/).

To run the tests, run the appropriate command below:

    pytest scrabble_score_test.py

If this does not work, you probably don't have pytest installed as it's own package.
So, alternatively, you can try to Python to run the pytest module:

    python3 -m pytest scrabble_score_test.py

If neither of these commands work, you'll have to install Pytest on your system.
If you're using debian, try:

    sudo apt-get install python3-pytest

To verify this has installed, try `pytest --help`, and you should see the Pytest
help message.

Consult your supervisor if you need help or once you have all the tests passing.


### Common `pytest` options

- `-v` : enable verbose output
- `-x` : stop running tests on first failure
- `--ff` : run failures from previous test before running other test cases

For other options, see `python -m pytest -h`

---

## Issue Tracking

Once you have identified an issue that you'd like to see resolved, you should capture
it in your project's Issue Tracker. GitLab groups an issue tracker in with the other
project's resources. It can be found listed on the side bar of your fork of the project
page - i.e. `https://gitlab.com/<your-namespace>/project2`. GitLab's issue tracker
is intuitive to use and there's little to be said here that'd help other than to say
dive right in and create an issue.

---

## Branching

### Overview
Branches are fundamental to Git work flows. They are lightweight and are trivial to create
and destroy. They allow code to be changed in isolation for the development of features and
the fixing of bugs/issues.

A repo will have a default branch, usually called `master`, but can be named anything at all
as there is nothing special about this branch other than its purpose. The `master` branch is
where to look for the most stable copy of the code and the point at which to begin if you'd
like to make any changes.

It is considered bad practice to push any changes directly to the `master` branch (GitLab
can prevent this by protecting the `master** branch**.

**IMPORTANT:** If you wish to make changes to a repo then the correct workflow is to create a
new branch off `master`, make changes there, push that branch, create a merge request and then
merge that branch back into the `master` branch. This is a brief overview of the process and
we'll use this project as an example of how to make changes in a repo.

### Creating a branch
After you've cloned the repo, you will be on the default (`master`) branch. You can confirm this
with `git status` where you should see something like:

    On branch master  
    Your branch is up to date with 'origin/master'.
    
    nothing to commit, working tree clean

Before you make any changes to any of the files, you should create a new branch so that
you can work safely. In Git, you use `git checkout <branch>` to swap between branches.
If we're creating a new branch, you need to tell Git that it's new and this is done with
the `-b` option.

A branch can be called anything you like but a good convention is to use `<username>/feature`.
For example:

    15:47 $ git checkout -b gthreepwood/loop_input
    Switched to a new branch 'gthreepwood/loop_input'

Any changes you make to the code now can be saved (committed) to your branch. It is good
practice to commit often, as it can allow you rollback changes if you opt to change your
approach to a problem.

Also, if you push these commits to a remote then it also means your work is backed-up.
We'll discussing pushing later.

Once you are happy with your changes you should use `git add` and `git commit` to apply
them to your branch. Refer back to
[using Git](https://gitlab.com/ct-starter-guide/how-to-git-going-in-foss/blob/master/tutorials/using_git.md)
if you're unsure about this.

---

## Writing The Code

Here comes the fun bit: editing [scrabble_score.py](scrabble_score.py), to fill in a definition for  the 'score' function.

### Letter Values

You'll need these:

```text
Letter                           Value
A, E, I, O, U, L, N, R, S, T       1
D, G                               2
B, C, M, P                         3
F, H, V, W, Y                      4
K                                  5
J, X                               8
Q, Z                               10
```

### Examples

"cabbage" should be scored as worth 14 points:

- 3 points for C
- 1 point for A, twice
- 3 points for B, twice
- 2 points for G
- 1 point for E

And to total:

- `3 + 2*1 + 2*3 + 2 + 1`
- = `3 + 2 + 6 + 3`
- = `5 + 9`
- = 14

### Possible Extensions

- You can play a double or a triple letter.

### Testing

When you think you have code that will pass some or all of the tests, save your work and run the tests. 
When you can pass all the tests, it's time to move on.

---

## Commit and Push

Once you're done writing code, you need to commit the changes to your new branch, and push the results 
back to GitLab.

### Commit Messages

When you make changes to a project, it is important that you articulate why you are
making these changes. This is done in the commit message.
[This](https://chris.beams.io/posts/git-commit/) is good overview of best practice
but can tl;dr'd as the following:

#### The seven rules of a great Git commit message

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. how

### Pushing your branch

Once you are you ready to submit your changes you should push your branch to the
remote Git repo (usually named `origin`). This is done with `git push origin <branch>`

    11:51 $ git push origin gthreepwood/loop_input 
    Enumerating objects: 3, done.
    Counting objects: 100% (3/3), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (2/2), 293 bytes | 293.00 KiB/s, done.
    Total 2 (delta 1), reused 0 (delta 0)
    remote: 
    remote: To create a merge request for gthreepwood/loop_input, visit:
    remote:   https://gitlab.com/ct-interns/project2/merge_requests/new?merge_request%5Bsource_branch%5D=gthreepwood%2Floop_input
    remote: 
    To gitlab.com:ct-interns/project2.git
    * [new branch]      gthreepwood/loop_input -> gthreepwood/loop_input

You'll notice that GitLab will return a URL for you to visit to set up a Merge
Request for this change. The MR is the formal way of asking the project maintainers
to *review* your code, and consider merging it into the `master` branch.


## Create Merge Request

To create the merge request, visit the URL that was returned when you pushed your branch
or return to your project fork within GitLab and select `Merge Requests` -> `New Merge Request`
from the side bar; which should take you to `https://gitlab.com/<your-namespace>/project2/merge_requests`.  
Follow the instructions laid out there, and be sure to link to the Issue that you
raised at the beginning of the process. This is done simply by `#<issue-number>`
and GitLab will do the rest.

## Code Review

Code review is an important part of the development process.
Projects often require independent review of each Pull Request from 2 engineers
before anything can be considered for merge.

This has many benefits:

- It allows fresh eyes onto a Merge Request to validate the code/approach and style.

- It improves the quality of the code as anything ambiguous can be questioned,
  discussed and clarified (either in code or in the supporting comments).

- It allows others to ensure that you have not skipped any essential parts of the
  software development process such as design documentation, unit tests, etc.

- Other engineers can become familiar with other parts of the project that they are
  not actively developing on.

- It can offer reassurance that the approach you've taken is sane and that your solution
  is good. It is common to begin coding through a problem, unsure if it is the best
  path - and then having it validated in review boosts confidence.

- Helps ensure that the engineer has understood the API of any other module in the system
  that they are calling or retrieving information from.

To review code properly takes time, and that means taking time out of
your current task to review other peoples code. This can be tough
sometimes when you're focused in on trying to solve a particular
problem, within a tight time frame.
However, there will always be time allocated for review on projects and consider
once you've completed your own MR it cannot be merged until another two engineers
have set some of their own time aside to review your work.

In summary, make noise about your reviews, bug your fellow engineers to review
your work. Review their work, learn about the project and be better programmers

-----


